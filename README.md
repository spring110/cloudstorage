# Cloud Storage Application
This application has three main functionalities:

1. **File Storage:** Upload/download/remove files
2. **Note Management:** Add/update/remove your personal text notes
3. **Password Management:** Save, edit, and delete your website credentials.  

This project was implemented in IntelliJ using Maven.
It uses Spring Boot application in the back-end, and Thymeleaf in the front-end.

The database is an in-memory database withing Spring framework.

Security is enabled in the app. You need to sign up with your own username and password first. After that you need to sign in with these credentials.
And after logging in, you can start using the app.
For the application screenshots, please see the 'screenshots' folder.

## How to start and the the app
- Run the CloudStorageApplication.java file. Since this is a Spring Boot application, running this file will start the entire application for you.
- Type the URL http://localhost:8080/home in your browser to go to the application.
