package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.*;
import com.udacity.jwdnd.course1.cloudstorage.services.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
public class HomeController {
    private CredentialService credentialService;
    private EncryptionService encryptionService;
    private FileService fileService;
    private NoteService noteService;
    private UserService userService;

    private List<File> fileList;
    private List<Note> noteList;
    private List<Credential> credentialList;

    public HomeController(CredentialService credentialService, EncryptionService encryptionService,
                          FileService fileService, NoteService noteService, UserService userService,
                          List<File> fileList, List<Note> noteList, List<Credential> credentialList) {
        System.out.println("Instantiating HomeController");
        this.credentialService = credentialService;
        this.encryptionService = encryptionService;
        this.fileService = fileService;
        this.noteService = noteService;
        this.userService = userService;

        this.fileList = fileList;
        this.noteList = noteList;
        this.credentialList = credentialList;
    }

    @PostConstruct
    public void postConstruct(){
        fileList = new ArrayList<>();
        noteList = new ArrayList<>();
        credentialList = new ArrayList<>();
    }

    @GetMapping("/home")
    public String getHomePage(Authentication authentication, Model model) {
        String username = authentication.getName();
        Integer userId = this.userService.getUser(username).getUserId();

        fileList = fileService.getFilesByUser(userId);
        noteList = noteService.getNotesByUser(userId);
        credentialList = credentialService.getCredentialsByUser(userId);

        model.addAttribute("fileList", fileList);
        model.addAttribute("noteList", noteList);
        model.addAttribute("credentialList", credentialList);
        model.addAttribute("encryptionService", this.encryptionService);  // so that I can use encryptionService object in home.html

        return "home";
    }

    // We need to initialize all the forms in home.html at the beginning
    // This is run at the beginning
    @ModelAttribute("newNote")
    public NoteForm createNoteForm () {
        return new NoteForm();
    }

    @ModelAttribute("newCredential")
    public CredentialForm createCredentialForm () {
        return new CredentialForm();
    }

    // File form does not exist - Therefore no need to do it
}
