package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mapper.NoteMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import com.udacity.jwdnd.course1.cloudstorage.model.NoteForm;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {
    private NoteMapper noteMapper;

    public NoteService(NoteMapper noteMapper) {
        System.out.println("Instantiating NoteService");
        this.noteMapper = noteMapper;
    }

    public List<Note> getNotesByUser(Integer userId) {
        return noteMapper.getAllNotesByUser(userId);
    }

    public Note getNote(Integer noteId) {
        return noteMapper.getNoteByNoteId(noteId);
    }

    public int addNote(NoteForm noteForm, Integer userId) {
        Integer noteId = null;  // this will be assigned by the database when inserted
        String noteTitle = noteForm.getNoteTitle();
        String noteDescription = noteForm.getNoteDescription();
        Integer userid = userId;

        Note note = new Note(noteId, noteTitle, noteDescription, userid);
         return noteMapper.insertNote(note);  // add the new note into database and return index number
    }

    public int updateNote(NoteForm noteForm) {
        Note existingNote = getNote(noteForm.getNoteId());
        existingNote.setNoteTitle(noteForm.getNoteTitle());
        existingNote.setNoteDescription(noteForm.getNoteDescription());
        return noteMapper.updateNote(existingNote);
    }

    public int deleteNote(Integer noteId) {
        return noteMapper.deleteNoteById(noteId);
    }

    public boolean isUniqueNoteName(String noteTitle, int userId){
        return noteMapper.getNote(noteTitle, userId) == null;
    }
}
