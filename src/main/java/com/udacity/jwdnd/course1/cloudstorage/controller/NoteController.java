package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import com.udacity.jwdnd.course1.cloudstorage.model.NoteForm;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class NoteController {
    NoteService noteService;
    UserService userService;

    public NoteController(NoteService noteService, UserService userService) {
        System.out.println("Instantiating NoteController");
        this.noteService = noteService;
        this.userService = userService;
    }

    @PostMapping("/add-note")
    public String postNote(@ModelAttribute("newNote") NoteForm noteForm, Authentication authentication, Model model) {
        String errorMessage = null;

        String username = authentication.getName();
        Integer userId = userService.getUser(username).getUserId();
        Integer noteId = noteForm.getNoteId();
        String noteTitle = noteForm.getNoteTitle();

        if (noteId == null) {  // add the note
            int rowIndex = noteService.addNote(noteForm, userId);
            if (rowIndex < 0) {
                errorMessage = "There was an error when adding your note.";
            }
        } else {  // update the note
            int rowIndex = noteService.updateNote(noteForm);
            if (rowIndex < 0) {
                errorMessage = "There was an error when updating your note.";
            }
        }

        if(errorMessage == null) {
            model.addAttribute("saveSuccess", true);
        } else {
            model.addAttribute("errorMessage", errorMessage);
        }

        return "result";
    }

    @GetMapping("/delete-note/{noteId}")
    public String deleteNote(@PathVariable int noteId, Model model) {
        String errorMessage = null;

        Integer deletedRowIndex = noteService.deleteNote(noteId);
        if(deletedRowIndex < 1){
            errorMessage = "Error! Note could not be deleted!";
        }

        if(errorMessage == null){
            model.addAttribute("saveSuccess", true);
        } else {
            model.addAttribute("errorMessage", errorMessage);
        }

        return "result";
    }
}
