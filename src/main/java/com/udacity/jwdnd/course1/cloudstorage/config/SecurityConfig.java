package com.udacity.jwdnd.course1.cloudstorage.config;

import com.udacity.jwdnd.course1.cloudstorage.services.AuthenticationService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration             // This is a config class!
@EnableWebSecurity         // This is a config class for Spring Security!
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private AuthenticationService authenticationService;

    public SecurityConfig(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    // Use our authenticationService to check user logins
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(this.authenticationService);
    }

    // How should the HTTP requests be handled?
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // signup page, css and jss files are permitted to public.
        // If any other URL comes in, authenticate it.
        http.authorizeRequests()
                .antMatchers("/signup", "/css/**", "/js/**").permitAll()
                .anyRequest().authenticated();

        // Our login form should be retrieved from /login endpoint, and it is permitted to public
        // /login endpoint returns login.html anyhow.
        // Here we are not using Spring Security's default login page, but our own.
        http.formLogin()
                .loginPage("/login")
                .permitAll();

        // Logout is handled here
        http.logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .permitAll();

        // When login is successful from the login form, go to /home endpoint.
        http.formLogin()
                .defaultSuccessUrl("/home", true);
    }

}
