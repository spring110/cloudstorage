package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mapper.FileMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import com.udacity.jwdnd.course1.cloudstorage.model.File;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class FileService {
    FileMapper fileMapper;

    public FileService(FileMapper fileMapper) {
        System.out.println("Instantiating FileService");
        this.fileMapper = fileMapper;
    }

    public List<File> getFilesByUser(Integer userId) {
        return fileMapper.getAllFilesByUser(userId);
    }

    public File getFileById(Integer fileId){
        return fileMapper.getFileById(fileId);
    }

    public int addFile(MultipartFile multipartFile, int userId) throws IOException {
        File file = new File(null, multipartFile.getOriginalFilename(), multipartFile.getContentType(),
                multipartFile.getSize(), userId, multipartFile.getBytes());
        return fileMapper.insertFile(file);
    }

    public boolean fileExists(String fileName, int userId) {
        return fileMapper.fileExists(fileName, userId);
    }

    public int deleteFile(Integer fileId, Integer userId) {
        return fileMapper.deleteFile(fileId, userId);
    }

}
