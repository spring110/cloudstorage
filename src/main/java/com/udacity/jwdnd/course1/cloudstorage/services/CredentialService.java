package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mapper.CredentialMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import com.udacity.jwdnd.course1.cloudstorage.model.CredentialForm;
import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import com.udacity.jwdnd.course1.cloudstorage.model.NoteForm;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;

@Service
public class CredentialService {
    private CredentialMapper credentialMapper;
    private EncryptionService encryptionService;

    public CredentialService(CredentialMapper credentialMapper, EncryptionService encryptionService) {
        System.out.println("Instantiating CredentialService");
        this.credentialMapper = credentialMapper;
        this.encryptionService = encryptionService;
    }

    public List<Credential> getCredentialsByUser(Integer userId) {
        return credentialMapper.getAllCredentialsByUser(userId);
    }

    public Credential getCredential(Integer credentialId) {
        return credentialMapper.getCredentialByCredentialId(credentialId);
    }

    public Credential getDecryptedCredential(Credential credential){
        String encryptedPassword = credential.getPassword();
        String key = credential.getKey();
        String decryptedPassword = encryptionService.decryptValue(encryptedPassword, key);
        credential.setPassword(decryptedPassword);
        return credential;
    }

    public int addCredential(CredentialForm credentialForm, Integer userId) {
        Integer credentialId = null;  // this will be assigned by the database when inserted
        String credentialUrl = credentialForm.getUrl();
        String credentialUsername = credentialForm.getUsername();
        String credentialPassword = credentialForm.getPassword();
        Integer userid = userId;

        SecureRandom random = new SecureRandom();
        byte[] key = new byte[16];
        random.nextBytes(key);
        String encodedKey = Base64.getEncoder().encodeToString(key);
        String encryptedPassword = encryptionService.encryptValue(credentialPassword, encodedKey);

        Credential newCredential = new Credential(credentialId, credentialUrl, credentialUsername, encodedKey, encryptedPassword, userid);
        return credentialMapper.insertCredential(newCredential);  // add the new note into database and return index number
    }

    public int updateCredential(CredentialForm credentialForm) {
        Credential existingCredential = getCredential(credentialForm.getCredentialId());  // take the existing credential

        String credentialPassword = credentialForm.getPassword();  // unencrypted value
        String dbKey = existingCredential.getKey();  // existing key is taken from db
        String encryptedPassword = encryptionService.encryptValue(credentialPassword, dbKey);  // password is encrypted with the dbKey

        existingCredential.setUrl(credentialForm.getUrl());
        existingCredential.setUsername(credentialForm.getUsername());
        existingCredential.setKey(dbKey);
        existingCredential.setPassword(encryptedPassword);

        return credentialMapper.updateCredential(existingCredential);
    }

    public int deleteCredential(Integer credentialId) {
        return credentialMapper.deleteCredentialById(credentialId);
    }

    public boolean isUniqueCredentialName(String credentialUrl, int userId){
        return credentialMapper.getCredential(credentialUrl, userId) == null;
    }
}
