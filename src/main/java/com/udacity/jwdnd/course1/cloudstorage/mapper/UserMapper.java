package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {

    // When I use userMapper.getUser(name) method, this will get the User object from db
    // If no user, then null is returned
    @Select("SELECT * FROM USERS WHERE username = #{username}")
    User getUser(String username);

    // When I use userMapper.insertUser(myuser) method, this will insert that User object into USERS table
    // It will return the userId of the User that was added
    @Insert("INSERT INTO USERS (username, salt, password, firstname, lastname) VALUES(#{username}, #{salt}, #{password}, #{firstName}, #{lastName})")
    @Options(useGeneratedKeys = true, keyProperty = "userId")
    int insertUser(User user);

}
