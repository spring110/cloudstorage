package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.CredentialForm;
import com.udacity.jwdnd.course1.cloudstorage.model.NoteForm;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.EncryptionService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CredentialController {
    CredentialService credentialService;
    UserService userService;
    EncryptionService encryptionService;

    public CredentialController(CredentialService credentialService, UserService userService, EncryptionService encryptionService) {
        System.out.println("Instantiating CredentialController");
        this.credentialService = credentialService;
        this.userService = userService;
        this.encryptionService = encryptionService;
    }

    @PostMapping("/add-credential")
    public String postCredential(@ModelAttribute("newCredential") CredentialForm credentialForm, Authentication authentication, Model model) {
        String errorMessage = null;

        String username = authentication.getName();
        Integer userId = userService.getUser(username).getUserId();
        Integer credentialId = credentialForm.getCredentialId();

        if (credentialId == null) {  // add the note
            int rowIndex = credentialService.addCredential(credentialForm, userId);
            if (rowIndex < 0) {
                errorMessage = "There was an error when adding your credential.";
            }
        } else {  // update the note
            int rowIndex = credentialService.updateCredential(credentialForm);
            if (rowIndex < 0) {
                errorMessage = "There was an error when updating your credential.";
            }
        }

        if(errorMessage == null) {
            model.addAttribute("saveSuccess", true);
        } else {
            model.addAttribute("errorMessage", errorMessage);
        }

        return "result";

    }

    @GetMapping("/delete-credential/{credentialId}")
    public String deleteCredential(@PathVariable int credentialId, Model model) {
        String errorMessage = null;

        Integer deletedRowIndex = credentialService.deleteCredential(credentialId);
        if(deletedRowIndex < 1){
            errorMessage = "Error! Credential could not be deleted!";
        }

        if(errorMessage == null){
            model.addAttribute("saveSuccess", true);
        } else {
            model.addAttribute("errorMessage", errorMessage);
        }

        return "result";
    }
}
