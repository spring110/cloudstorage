package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.File;
import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
public class FileController {
    FileService fileService;
    UserService userService;

    public FileController(FileService fileService, UserService userService) {
        System.out.println("Instantiating FileController");
        this.fileService = fileService;
        this.userService = userService;
    }

    @GetMapping("/download-file/{fileId}")
    public ResponseEntity downloadFile(@PathVariable int fileId, Authentication authentication, Model model) {
        String username = authentication.getName();
        Integer userId = userService.getUser(username).getUserId();
        File file = fileService.getFileById(fileId);

        if(file != null) {
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(file.getContentType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\""+ file.getFileName() +"\"")
                    .body(file.getFileData());
        }
        return null;
    }

    @PostMapping("/add-file")
    public String postFile(@RequestParam("fileUpload") MultipartFile file, Authentication authentication, Model model) throws IOException {
        String errorMessage = null;

        String username = authentication.getName();
        Integer userId = userService.getUser(username).getUserId();

        if(fileService.fileExists(file.getOriginalFilename(), userId) == true){
            errorMessage = "Error! File name is not unique";
        }

        if(file.isEmpty()){
            errorMessage = "Error! No file was selected";
        }

        if(errorMessage == null){
            try {
                int rowIndex = fileService.addFile(file, userId);
                if(rowIndex < 0){
                    errorMessage = "There was an error uploading your file.";
                }
            } catch (IOException ioException){
                errorMessage = "There was an error uploading your file.";
            }
        }

        if(errorMessage == null) {
            model.addAttribute("saveSuccess", true);

        } else {
            model.addAttribute("errorMessage", errorMessage);
        }

        return "result";
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleFileSizeLimitExceeded(MaxUploadSizeExceededException exc) {
        return "{\"error\":\"file too big\"}";
    }

    @GetMapping("/delete-file/{fileId}")
    public String deleteFile(@PathVariable int fileId, Authentication authentication, Model model) {
        String errorMessage = null;

        String username = authentication.getName();
        Integer userId = userService.getUser(username).getUserId();

        int deletedRowIndex = fileService.deleteFile(fileId, userId);

        if(deletedRowIndex < 1){
            errorMessage = "Error! File could not be deleted!";
        }

        if(errorMessage == null){
            model.addAttribute("saveSuccess", true);
        } else {
            model.addAttribute("errorMessage", errorMessage);
        }

        return "result";
    }
}
