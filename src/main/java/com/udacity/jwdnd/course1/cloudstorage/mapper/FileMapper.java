package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.model.File;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface FileMapper {

    @Select("SELECT * FROM FILES WHERE userid = #{userId}")
    List<File> getAllFilesByUser(Integer userId);

    @Select("SELECT * FROM FILES WHERE fileid = #{fileId}")
    File getFileById(Integer fileId);

    @Select("SELECT * FROM FILES WHERE filename = #{fileName} and userid = #{userId}}")
    File getFileByName(String fileName, int userId);

    @Select("SELECT CASE WHEN EXISTS (" +
            "SELECT * FROM FILES WHERE fileName=#{fileName} AND userId=#{userId}" +
            ") " +
            "THEN TRUE " +
            "ELSE FALSE END AS bool;")
    boolean fileExists(String fileName, Integer userId);

    @Insert("INSERT INTO FILES (filename, contenttype, filesize, userid, filedata) VALUES(#{fileName}, #{contentType}, #{fileSize}, #{userId}, #{fileData})")
    @Options(useGeneratedKeys = true, keyProperty = "fileId")
    int insertFile(File file);

    @Update("UPDATE FILES SET filename=#{fileName}, contenttype=#{contentType}, filesize=#{fileSize}, filedata=#{fileData} " +
            "WHERE fileid=#{fileId}")
    public int updateFile(File file);

    @Delete("DELETE FROM FILES WHERE fileid = #{fileId} and userid = #{userId}")
    public int deleteFile(Integer fileId, Integer userId);

}
