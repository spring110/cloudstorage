package com.udacity.jwdnd.course1.cloudstorage;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CloudStorageApplicationTests {

	@LocalServerPort
	private int port;

	private String baseURL;

	private WebDriver driver;
	private SignupPage signupPage;
	private LoginPage loginPage;
	private HomePage homePage;
	private ResultPage resultPage;

	@BeforeAll
	static void beforeAll() {
		WebDriverManager.chromedriver().setup();
	}

	@BeforeEach
	public void beforeEach() {
		this.driver = new ChromeDriver();
		baseURL = "http://localhost:" + port;
	}

	@AfterEach
	public void afterEach() {
		if (this.driver != null) {
			driver.quit();
		}
	}

	public void signupUser() {
		driver.get(baseURL + "/signup");
		signupPage = new SignupPage(driver);
		signupPage.createUser("John", "Adam", "johnadam", "password1");
	}

	public void loginUser() {
		driver.get(baseURL + "/login");
		loginPage = new LoginPage(driver);
		loginPage.loginUser("johnadam", "password1");
	}

	@Test
	@Order(1)
	public void unauthorizedAccessToPagesTest(){
		driver.get(baseURL+"/home");
		// checks that the unauthorized user cannot access the home page
		assertEquals("Login", driver.getTitle());

		driver.get(baseURL+"/signup");
		// checks that the unauthorized user can access the signup page
		assertEquals("Sign Up", driver.getTitle());
	}

	@Test
	@Order(2)
	public void signupAndLoginTest() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);

		// checks if the signup is successful
		signupUser();
		wait.until(ExpectedConditions.titleIs("Login"));
		loginPage = new LoginPage(driver); // Signup page redirects to Login page, therefore Login page should be checked
		assertTrue(loginPage.isSignupSuccessful());

		// checks if the signed-up username can access the home page
		loginUser();
		wait.until(ExpectedConditions.titleIs("Home"));
		assertEquals("Home", driver.getTitle());

		// checks if the logged-in user can logout and cannot access
		// the home page anymore
		homePage = new HomePage(driver);
		Thread.sleep(2000);
		homePage.logout();
		Thread.sleep(2000);

		driver.get(baseURL+"/home");
		assertEquals("Login", driver.getTitle()); // the user should be redirected to login page
	}

	@Test
	@Order(3)
	public void createNoteTest() throws InterruptedException {
		loginUser();
		String title = "To-do 1";
		String description = "Buy groceries!";
		createNote(title, description);

		// Verify that the note is displayed
		assertEquals(title, homePage.getNoteTitleText());
		assertEquals(description, homePage.getNoteDescriptionText());

		homePage.clickDeleteNote(); // delete the note since there's no further use
	}

	public void createNote(String title, String description) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		homePage = new HomePage(driver);

		wait.until(ExpectedConditions.visibilityOf(homePage.getNotesTab()));  // wait until the notes tab is visible
		Thread.sleep(2000);
		homePage.openNotesTab();

		wait.until(ExpectedConditions.elementToBeClickable(homePage.getAddNoteButton())); // wait until the add note button is visible
		homePage.clickAddNote();

		wait.until(ExpectedConditions.visibilityOf(homePage.getSaveNoteButton())); // wait until the note submit button is visible

		// Create with the data
		homePage.createNewNote(title, description);

		wait.until(ExpectedConditions.titleIs("Result"));  // wait until the result page is shown
		resultPage = new ResultPage(driver);
		assertEquals("Success", resultPage.getStatus()); // Test the success

		driver.get(baseURL + "/home"); // go back to home page
		wait.until(ExpectedConditions.titleIs("Home"));  // wait until the home page is shown
		homePage.openNotesTab(); // opens the note tab

		wait.until(ExpectedConditions.visibilityOf(homePage.getNoteTitleElement())); // wait until the note title is shown
		wait.until(ExpectedConditions.visibilityOf(homePage.getNoteDescriptionElement())); // wait until the note desc is shown
	}

	@Test
	@Order(4)
	public void editNoteTest() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		loginUser();  // login in case the user is logged out
		String title = "To-do 2";
		String description = "Read the novel!";
		createNote(title, description);

		homePage.clickEditNote();
		wait.until(ExpectedConditions.visibilityOf(homePage.getSaveNoteButton())); // wait until the note submit button is visible
		description = "No, read the story book!";
		homePage.editNote(title, description);

		wait.until(ExpectedConditions.titleIs("Result"));  // wait until the result page is shown
		resultPage = new ResultPage(driver);
		assertEquals("Success", resultPage.getStatus()); // Test the success

		driver.get(baseURL + "/home"); // go back to home page
		wait.until(ExpectedConditions.titleIs("Home"));  // wait until the home page is shown
		homePage.openNotesTab(); // opens the note tab

		wait.until(ExpectedConditions.visibilityOf(homePage.getNoteTitleElement())); // wait until the note title is shown
		wait.until(ExpectedConditions.visibilityOf(homePage.getNoteDescriptionElement())); // wait until the note desc is shown

		// Verify that the changes are display
		assertEquals(title, homePage.getNoteTitleText());
		assertEquals(description, homePage.getNoteDescriptionText());

		homePage.clickDeleteNote(); // delete the note since there's no further use
	}

	@Test
	@Order(5)
	public void deleteNoteTest() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		loginUser();  // login in case the user is logged out
		String title = "To-do 3";
		String description = "Cook lentils!";
		createNote(title, description);

		homePage.clickDeleteNote();
		wait.until(ExpectedConditions.titleIs("Result"));  // wait until the result page is shown
		resultPage = new ResultPage(driver);
		assertEquals("Success", resultPage.getStatus()); // Test the success

		driver.get(baseURL + "/home"); // go back to home page
		wait.until(ExpectedConditions.titleIs("Home"));  // wait until the home page is shown
		homePage.openNotesTab(); // opens the note tab
		Thread.sleep(2000);

		// Verify that the note is no longer displayed
		assertThrows(Exception.class, () -> homePage.getNoteTitleElement().click());
		assertThrows(Exception.class, () -> homePage.getNoteDescriptionElement().click());
	}

	@Test
	@Order(6)
	public void createCredentialTest() throws InterruptedException {
		loginUser(); // login in case the user is logged out
		String url = "www.example.net";
		String username = "admin";
		String password = "123456";
		createCredential(url, username, password);

		// Verify that the credential is displayed
		// Verify also that the password is displayed as encrypted
		assertEquals(url, homePage.getCredentialUrlText());
		assertEquals(username, homePage.getCredentialUsernameText());
		assertNotEquals(password, homePage.getCredentialPasswordText());

		homePage.clickDeleteCredential(); // delete the note since there's no further use
	}

	public void createCredential(String url, String username, String password) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		homePage = new HomePage(driver);

		wait.until(ExpectedConditions.visibilityOf(homePage.getCredentialsTab()));  // wait until the credentials tab is visible
		Thread.sleep(2000);
		homePage.openCredentialsTab();

		wait.until(ExpectedConditions.elementToBeClickable(homePage.getAddCredentialButton())); // wait until the add credential button is visible
		homePage.clickAddCredential();

		wait.until(ExpectedConditions.visibilityOf(homePage.getSaveCredentialButton())); // wait until the credential save button is visible

		// Create with the data
		homePage.createNewCredential(url, username, password);

		wait.until(ExpectedConditions.titleIs("Result"));  // wait until the result page is shown
		resultPage = new ResultPage(driver);
		assertEquals("Success", resultPage.getStatus()); // Test the success

		driver.get(baseURL + "/home"); // go back to home page
		wait.until(ExpectedConditions.titleIs("Home"));  // wait until the home page is shown
		homePage.openCredentialsTab(); // opens the note tab

		wait.until(ExpectedConditions.visibilityOf(homePage.getCredentialUrlElement())); // wait until the credential url is shown
		wait.until(ExpectedConditions.visibilityOf(homePage.getCredentialUsernameElement())); // wait until credential username is shown
		wait.until(ExpectedConditions.visibilityOf(homePage.getCredentialPasswordElement())); // wait until credential password is shown
	}

	@Test
	@Order(7)
	public void editCredentialTest() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		loginUser();  // login in case the user is logged out
		String url = "www.example.net";
		String username = "admim";  // typo
		String password = "123456";
		createCredential(url, username, password);

		homePage.clickEditCredential();
		wait.until(ExpectedConditions.visibilityOf(homePage.getSaveCredentialButton())); // wait until the save credential button is visible
		Thread.sleep(2000);

		// Verify that the viewable password is unencrypted
		assertEquals(password, homePage.getCredentialPasswordElementInForm().getAttribute("value"));

		username = "admin"; // correction
		homePage.editCredential(url, username, password);

		wait.until(ExpectedConditions.titleIs("Result"));  // wait until the result page is shown
		resultPage = new ResultPage(driver);
		assertEquals("Success", resultPage.getStatus()); // Test the success

		driver.get(baseURL + "/home"); // go back to home page
		wait.until(ExpectedConditions.titleIs("Home"));  // wait until the home page is shown
		homePage.openCredentialsTab(); // opens the credential tab

		wait.until(ExpectedConditions.visibilityOf(homePage.getCredentialUrlElement())); // wait until the credential url is shown
		wait.until(ExpectedConditions.visibilityOf(homePage.getCredentialUsernameElement())); // wait until credential username is shown
		wait.until(ExpectedConditions.visibilityOf(homePage.getCredentialPasswordElement())); // wait until credential password is shown

		// Verify that the changes are displayed
		assertEquals(url, homePage.getCredentialUrlText());
		assertEquals(username, homePage.getCredentialUsernameText());
		assertNotEquals(password, homePage.getCredentialPasswordText());

		homePage.clickDeleteCredential(); // delete the credential since there's no further use
	}

	@Test
	@Order(8)
	public void deleteCredentialTest() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		loginUser();  // login in case the user is logged out
		String url = "www.example.net";
		String username = "superuser";
		String password = "s123456";
		createCredential(url, username, password);

		homePage.clickDeleteCredential();
		wait.until(ExpectedConditions.titleIs("Result"));  // wait until the result page is shown
		resultPage = new ResultPage(driver);
		assertEquals("Success", resultPage.getStatus()); // Test the success

		driver.get(baseURL + "/home"); // go back to home page
		wait.until(ExpectedConditions.titleIs("Home"));  // wait until the home page is shown
		homePage.openCredentialsTab(); // opens the credentials tab
		Thread.sleep(2000);

		// Verifies that the credential is no longer displayed
		assertThrows(Exception.class, () -> homePage.getCredentialUrlElement().click());
		assertThrows(Exception.class, () -> homePage.getCredentialUsernameElement().click());
		assertThrows(Exception.class, () -> homePage.getCredentialPasswordElement().click());
	}
}
