package com.udacity.jwdnd.course1.cloudstorage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    public HomePage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    // *** Logout section *** //

    @FindBy(id = "logout-button")
    private WebElement logoutButton;

    public void logout(){
        logoutButton.click();
    }

    // **** Tabs section *** //

    @FindBy(id = "nav-notes-tab")
    private WebElement notesTab;

    @FindBy(id = "nav-credentials-tab")
    private WebElement credentialsTab;

    public void openNotesTab(){
        notesTab.click();
    }

    public void openCredentialsTab(){
        credentialsTab.click();
    }

    public WebElement getNotesTab() {
        return notesTab;
    }

    public WebElement getCredentialsTab() {
        return credentialsTab;
    }

    // *** Notes section *** //

    @FindBy(id = "add-note")
    private WebElement addNoteButton;

    @FindBy(id = "note-title")
    private WebElement noteTitleInForm;

    @FindBy(id = "note-description")
    private WebElement noteDescriptionInForm;

    @FindBy(id = "save-note")
    private WebElement saveNoteButtonInForm;

    @FindBy(xpath = "//*[@id='userTable']/tbody/tr/th")
    private WebElement noteTitle;

    @FindBy(xpath = "//*[@id='userTable']/tbody/tr/td[2]")
    private WebElement noteDescription;

    @FindBy(xpath = "//*[@id='userTable']/tbody/tr/td[1]/button")
    private WebElement editNoteButton;

    @FindBy(xpath = "//*[@id='userTable']/tbody/tr/td[1]/a")
    private WebElement deleteNoteButton;

    public WebElement getAddNoteButton() {
        return addNoteButton;
    }

    public WebElement getSaveNoteButton() { return saveNoteButtonInForm; }

    public WebElement getNoteTitleElement() { return noteTitle; }

    public WebElement getNoteDescriptionElement() { return noteDescription; }

    public void createNewNote(String title, String description){
        this.noteTitleInForm.sendKeys(title);
        this.noteDescriptionInForm.sendKeys(description);
        this.saveNoteButtonInForm.click();
    }

    public void editNote(String title, String description){
        this.noteTitleInForm.clear();
        this.noteDescriptionInForm.clear();
        createNewNote(title, description);
    }

    public String getNoteTitleText() { return noteTitle.getText(); }

    public String getNoteDescriptionText() {
        return noteDescription.getText();
    }

    public void clickAddNote() {
        addNoteButton.click();
    }

    public void clickEditNote() {
        editNoteButton.click();
    }

    public void clickDeleteNote(){
        deleteNoteButton.click();
    }

    // *** Credentials section *** //

    @FindBy(id = "add-credential")
    private WebElement addCredentialButton;

    @FindBy(id = "credential-url")
    private WebElement credentialUrlInForm;

    @FindBy(id = "credential-username")
    private WebElement credentialUsernameInForm;

    @FindBy(id = "credential-password")
    private WebElement credentialPasswordInForm;

    @FindBy(id = "save-credential")
    private WebElement saveCredentialButton;

    @FindBy(xpath = "//*[@id='credentialTable']/tbody/tr/th")
    private WebElement credentialUrl;

    @FindBy(xpath = "//*[@id='credentialTable']/tbody/tr/td[2]")
    private WebElement credentialUsername;

    @FindBy(xpath = "//*[@id='credentialTable']/tbody/tr/td[3]")
    private WebElement credentialPassword;

    @FindBy(xpath = "//*[@id='credentialTable']/tbody/tr/td[1]/button")
    private WebElement editCredentialButton;

    @FindBy(xpath = "//*[@id='credentialTable']/tbody/tr/td[1]/a")
    private WebElement deleteCredentialButton;

    public WebElement getAddCredentialButton() {
        return addCredentialButton;
    }

    public WebElement getSaveCredentialButton() { return saveCredentialButton; }

    public WebElement getCredentialUrlElement() { return credentialUrl; }

    public WebElement getCredentialUsernameElement() { return credentialUsername; }

    public WebElement getCredentialPasswordElement() { return credentialPassword; }

    public WebElement getCredentialPasswordElementInForm() { return credentialPasswordInForm; }

    public void createNewCredential(String url, String username, String password){
        credentialUrlInForm.sendKeys(url);
        credentialUsernameInForm.sendKeys(username);
        credentialPasswordInForm.sendKeys(password);
        saveCredentialButton.click();
    }

    public void editCredential(String url, String username, String password){
        // clear fields
        credentialUrlInForm.clear();
        credentialUsernameInForm.clear();
        credentialPasswordInForm.clear();

        // populate fields
        createNewCredential(url, username, password);
    }

    public String getCredentialUrlText() { return credentialUrl.getText(); }

    public String getCredentialUsernameText(){
        return credentialUsername.getText();
    }

    public String getCredentialPasswordText(){
        return credentialPassword.getText();
    }

    public void clickAddCredential(){
        addCredentialButton.click();
    }

    public void clickEditCredential(){
        editCredentialButton.click();
    }

    public void clickDeleteCredential(){
        deleteCredentialButton.click();
    }
}
