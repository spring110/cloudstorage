package com.udacity.jwdnd.course1.cloudstorage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    @FindBy(id = "inputUsername")
    private WebElement usernameField;

    @FindBy(id = "inputPassword")
    private WebElement passwordField;

    @FindBy(id = "login-button")
    private WebElement loginButton;

    @FindBy(id = "signup-success-msg")
    private WebElement signupSuccessMessage;

    @FindBy(id = "login-error-msg")
    private WebElement invalidCredMessage;

    @FindBy(id = "logout-msg")
    private WebElement logoutMessage;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void loginUser(String username, String password) {
        // clear fields
        this.usernameField.clear();
        this.passwordField.clear();

        this.usernameField.sendKeys(username);
        this.passwordField.sendKeys(password);
        this.loginButton.click();
    }

    public boolean isSignupSuccessful(){
        return signupSuccessMessage.isDisplayed();
    }
}
