package com.udacity.jwdnd.course1.cloudstorage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignupPage {

    @FindBy(id = "inputFirstName")
    private WebElement firstNameField;

    @FindBy(id = "inputLastName")
    private WebElement lastNameField;

    @FindBy(id = "inputUsername")
    private WebElement usernameField;

    @FindBy(id = "inputPassword")
    private WebElement passwordField;

    @FindBy(id = "SignupButton")
    private WebElement signupButton;

    @FindBy(id = "login-link-1")
    private WebElement loginLink;

    @FindBy(id = "signup-success-msg")
    private WebElement signupSuccess;

    @FindBy(id = "signup-error-msg")
    private WebElement signupFail;

    public SignupPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void createUser(String firstName, String lastName, String username, String password){
        // clear fields
        this.firstNameField.clear();
        this.lastNameField.clear();
        this.usernameField.clear();
        this.passwordField.clear();

        // populate fields
        this.firstNameField.sendKeys(firstName);
        this.lastNameField.sendKeys(lastName);
        this.usernameField.sendKeys(username);
        this.passwordField.sendKeys(password);
        this.signupButton.click();
    }

    public boolean isSuccessful(){
        return signupSuccess.isDisplayed();
    }

    public boolean isFailed(){
        return signupFail.isDisplayed();
    }
}
